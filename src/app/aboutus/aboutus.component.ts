import { Component, OnInit, Input } from '@angular/core';
import { ChangeDetectionStrategy } from '@angular/core';
import { CalendarEvent, CalendarView } from 'angular-calendar';

@Component({
  selector: 'app-aboutus',
  templateUrl: './aboutus.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  styleUrls: ['./aboutus.component.css']
})
export class AboutusComponent implements OnInit {

  view: CalendarView = CalendarView.Month;
  @Input() locale: string = 'en';
  viewDate: Date = new Date();

  events: CalendarEvent[] = [];

  constructor() { }

  ngOnInit() {
  }

  changeDay(date: Date) {
    this.viewDate = date;
    this.view = CalendarView.Day;
  }

}
