import { DataSource } from '@angular/cdk/collections';
import { MatPaginator } from '@angular/material/paginator';
import { MatSort } from '@angular/material/sort';
import { map } from 'rxjs/operators';
import { Observable, of as observableOf, merge } from 'rxjs';

// TODO: Replace this with your own data model type
export interface ProjectDataItem {
  name: string;
  client: string;
  status: string;
  description: string;
}

// TODO: replace this with real data from your application
const EXAMPLE_DATA: ProjectDataItem[] = [
  {
    name: 'OWTI Monitoring System',
    client: 'Shree Abirami Enggineering Works',
    status: 'Completed',
    description: 'Online Winding Temperature Monitoring System',
  },
  {
    name: 'Wireless Motor Monitor',
    client: 'RAACTS(Ramco)',
    status: 'Completed',
    description: 'Wireless Motor Monitor',
  },
  {
    name: 'Soft Printer',
    client: 'Future Focus Infotech',
    status: 'Completed',
    description: 'Soft Printer',
  },
  {
    name: 'RF based water level controller',
    client: 'Arka Automations',
    status: 'In-Progress',
    description: 'RF based water level controller',
  },
  {
    name: 'Wifi based ware-house monitor',
    client: 'Arka Automations',
    status: 'Completed',
    description: 'Wifi based ware-house monitor',
  },
  {
    name: 'RF Gate Controller',
    client: 'GOMS',
    status: 'In-Progress',
    description: 'RF Gate Controller',
  },
  {
    name: 'High Voltage ShotGun',
    client: 'GOMS',
    status: 'In-Progress',
    description: 'High Voltage ShotGun',
  },
  {
    name: 'BLDC Motor Driver',
    client: 'Manivannan',
    status: 'In-Progress',
    description: 'BLDC Motor Driver',
  },
  {
    name: 'RealTime Monitor Application',
    client: 'New Silicon Technologies',
    status: 'Completed',
    description: 'RealTime Monitor Application',
  },
  {
    name: 'Motor Driver Controller',
    client: 'Leemetal Marketing',
    status: 'Completed',
    description: 'Motor Driver Controller',
  },
  {
    name: 'Pulse Controller',
    client: 'Anthony',
    status: 'Completed',
    description: 'Pulse Controller'
  },
  {
    name: 'Power Supply Unit',
    client: 'PVLN Healthcare',
    status: 'Completed',
    description: 'Power Supply Unit'
  },
  {
    name: 'Piezo Based Ghatam',
    client: 'Guru Prasad',
    status: 'In-Progress',
    description: 'Piezo Based Ghatam'
  },
  {
    name: 'Double Boost Audio Amplifier',
    client: 'Shankar',
    status: 'Completed',
    description: 'Double Boost Audio Amplifier'
  },
  {
    name: 'Lift Controller TEST JIG',
    client: 'High Tech Lifts',
    status: 'Completed',
    description: 'Lift Controller TEST JIG'
  },
  {
    name: 'USB PIC Programmer',
    client: 'Asaimani',
    status: 'Completed',
    description: 'USB PIC Programmer'
  },
  {
    name: 'Serial PIC Programmer',
    client: 'Asaimani',
    status: 'Completed',
    description: 'Serial Programmers for PIC use In Circuit Serial Programming (ICSP) for the burning purposes. They can be used to burn the hex code without removing the Microcontroller from the application circuitry.'
  },
];

/**
 * Data source for the ProjectData view. This class should
 * encapsulate all logic for fetching and manipulating the displayed data
 * (including sorting, pagination, and filtering).
 */
export class ProjectDataDataSource extends DataSource<ProjectDataItem> {
  data: ProjectDataItem[] = EXAMPLE_DATA;
  paginator: MatPaginator;
  sort: MatSort;

  constructor() {
    super();
  }

  /**
   * Connect this data source to the table. The table will only update when
   * the returned stream emits new items.
   * @returns A stream of the items to be rendered.
   */
  connect(): Observable<ProjectDataItem[]> {
    // Combine everything that affects the rendered data into one update
    // stream for the data-table to consume.
    const dataMutations = [
      observableOf(this.data),
      this.paginator.page,
      this.sort.sortChange
    ];

    return merge(...dataMutations).pipe(map(() => {
      return this.getPagedData(this.getSortedData([...this.data]));
    }));
  }

  /**
   *  Called when the table is being destroyed. Use this function, to clean up
   * any open connections or free any held resources that were set up during connect.
   */
  disconnect() {}

  /**
   * Paginate the data (client-side). If you're using server-side pagination,
   * this would be replaced by requesting the appropriate data from the server.
   */
  private getPagedData(data: ProjectDataItem[]) {
    const startIndex = this.paginator.pageIndex * this.paginator.pageSize;
    return data.splice(startIndex, this.paginator.pageSize);
  }

  /**
   * Sort the data (client-side). If you're using server-side sorting,
   * this would be replaced by requesting the appropriate data from the server.
   */
  private getSortedData(data: ProjectDataItem[]) {
    if (!this.sort.active || this.sort.direction === '') {
      return data;
    }

    return data.sort((a, b) => {
      const isAsc = this.sort.direction === 'asc';
      switch (this.sort.active) {
        case 'name': return compare(a.name, b.name, isAsc);
        case 'client': return compare(a.client, b.client, isAsc);
        case 'status': return compare(a.status, b.status, isAsc);
        default: return 0;
      }
    });
  }
}

/** Simple sort comparator for example ID/Name columns (for client-side sorting). */
function compare(a:string, b:string, isAsc) {
  return (a < b ? -1 : 1) * (isAsc ? 1 : -1);
}
