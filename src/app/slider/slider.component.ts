import { Component, OnInit } from '@angular/core';
import { OwlOptions } from 'ngx-owl-carousel-o';
@Component({
  selector: 'app-slider',
  templateUrl: './slider.component.html',
  styleUrls: ['./slider.component.css']
})
export class SliderComponent implements OnInit {

  Images = ['../../assets/img/slide/img1.jpg',
  '../../assets/img/slide/img2.jpg',
  '../../assets/img/slide/img3.jpg',
  '../../assets/img/slide/img4.jpg',
  '../../assets/img/slide/img5.jpg',
  '../../assets/img/slide/img6.jpg'];  
  constructor() { }

  title = 'angularowlslider';
  customOptions: any = {
    loop: true,
    mouseDrag: false,
    touchDrag: true,
    pullDrag: false,
    dots: false,
    navSpeed: 700,
    autoplay:true,
    navText: ['', ''],
    responsive: {
      0: {
        items: 1
      },
      400: {
        items: 2
      },
      740: {
        items: 3
      },
      940: {
        items: 3
      }
    },
    nav: true
  }

  ngOnInit() {
    
  }

}
